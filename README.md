# Docker Symfony

## Install your own GitLab Runner

How to install GitLab Runner: [link](https://docs.gitlab.com/runner/install/linux-manually.html "Install GitLab Runner") (Tuto : [GitLab](https://about.gitlab.com/blog/2016/03/01/gitlab-runner-with-docker/))

For Debian/Ubuntu:

Find your architecture : `dpkg --print-architecture`

```shell
curl -LJO https://gitlab-runner-downloads.s3.amazonaws.com/latest/deb/gitlab-runner_<arch>.deb
sudo dpkg -i gitlab-runner_<arch>.deb
```

Config the Runner:

```shell
# Find the gitlab-ci token for this runner (Project > Settings > CI / CD > Set up a specific Runner manually)
sudo gitlab-runner register \
  --non-interactive \
  --url "https://gitlab.com/" \
  --registration-token "PROJECT_REGISTRATION_TOKEN" \
  --executor "docker" \
  --docker-image alpine:latest \
  --description "docker-runner" \
  --tag-list "docker,aws" \
  --run-untagged="true" \
  --locked="false" \
  --access-level="not_protected"
```

Run the Runner:

```shell

```